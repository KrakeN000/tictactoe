﻿namespace Tic_Tac_Toe
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Theme = new System.Windows.Forms.GroupBox();
            this.Night = new System.Windows.Forms.RadioButton();
            this.Day = new System.Windows.Forms.RadioButton();
            this.AIDifficult = new System.Windows.Forms.GroupBox();
            this.Hard = new System.Windows.Forms.Label();
            this.Difficult = new System.Windows.Forms.TrackBar();
            this.Middle = new System.Windows.Forms.Label();
            this.Easy = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.PlayerName = new System.Windows.Forms.TextBox();
            this.NamePl = new System.Windows.Forms.GroupBox();
            this.langBox = new System.Windows.Forms.ComboBox();
            this.Language = new System.Windows.Forms.GroupBox();
            this.Theme.SuspendLayout();
            this.AIDifficult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Difficult)).BeginInit();
            this.NamePl.SuspendLayout();
            this.Language.SuspendLayout();
            this.SuspendLayout();
            // 
            // Theme
            // 
            this.Theme.Controls.Add(this.Night);
            this.Theme.Controls.Add(this.Day);
            this.Theme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Theme.Location = new System.Drawing.Point(12, 12);
            this.Theme.Name = "Theme";
            this.Theme.Size = new System.Drawing.Size(222, 70);
            this.Theme.TabIndex = 1;
            this.Theme.TabStop = false;
            this.Theme.Text = "Тема";
            // 
            // Night
            // 
            this.Night.AutoSize = true;
            this.Night.FlatAppearance.BorderSize = 0;
            this.Night.Location = new System.Drawing.Point(6, 42);
            this.Night.Name = "Night";
            this.Night.Size = new System.Drawing.Size(64, 17);
            this.Night.TabIndex = 2;
            this.Night.Text = "Тёмная";
            this.Night.UseVisualStyleBackColor = true;
            // 
            // Day
            // 
            this.Day.AutoSize = true;
            this.Day.Checked = true;
            this.Day.FlatAppearance.BorderSize = 0;
            this.Day.Location = new System.Drawing.Point(6, 19);
            this.Day.Name = "Day";
            this.Day.Size = new System.Drawing.Size(67, 17);
            this.Day.TabIndex = 0;
            this.Day.TabStop = true;
            this.Day.Text = "Светлая";
            this.Day.UseVisualStyleBackColor = true;
            // 
            // AIDifficult
            // 
            this.AIDifficult.Controls.Add(this.Hard);
            this.AIDifficult.Controls.Add(this.Difficult);
            this.AIDifficult.Controls.Add(this.Middle);
            this.AIDifficult.Controls.Add(this.Easy);
            this.AIDifficult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AIDifficult.Location = new System.Drawing.Point(12, 88);
            this.AIDifficult.Name = "AIDifficult";
            this.AIDifficult.Size = new System.Drawing.Size(222, 97);
            this.AIDifficult.TabIndex = 2;
            this.AIDifficult.TabStop = false;
            this.AIDifficult.Text = "Уровень сложности ИИ";
            // 
            // Hard
            // 
            this.Hard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Hard.Location = new System.Drawing.Point(170, 67);
            this.Hard.Name = "Hard";
            this.Hard.Size = new System.Drawing.Size(46, 13);
            this.Hard.TabIndex = 5;
            this.Hard.Text = "Сложно";
            this.Hard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Difficult
            // 
            this.Difficult.Location = new System.Drawing.Point(6, 19);
            this.Difficult.Maximum = 2;
            this.Difficult.Name = "Difficult";
            this.Difficult.Size = new System.Drawing.Size(210, 45);
            this.Difficult.TabIndex = 0;
            this.Difficult.Value = 1;
            // 
            // Middle
            // 
            this.Middle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Middle.Location = new System.Drawing.Point(89, 67);
            this.Middle.Name = "Middle";
            this.Middle.Size = new System.Drawing.Size(44, 13);
            this.Middle.TabIndex = 4;
            this.Middle.Text = "Средне";
            this.Middle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Easy
            // 
            this.Easy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Easy.Location = new System.Drawing.Point(6, 67);
            this.Easy.Name = "Easy";
            this.Easy.Size = new System.Drawing.Size(38, 13);
            this.Easy.TabIndex = 3;
            this.Easy.Text = "Легко";
            this.Easy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Save.FlatAppearance.BorderSize = 0;
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.Location = new System.Drawing.Point(12, 354);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(105, 45);
            this.Save.TabIndex = 6;
            this.Save.Text = "Сохранить";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Back
            // 
            this.Back.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Back.FlatAppearance.BorderSize = 0;
            this.Back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Back.Location = new System.Drawing.Point(129, 354);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(105, 45);
            this.Back.TabIndex = 7;
            this.Back.Text = "Отмена";
            this.Back.UseVisualStyleBackColor = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // Reset
            // 
            this.Reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Reset.FlatAppearance.BorderSize = 0;
            this.Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reset.Location = new System.Drawing.Point(12, 316);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(222, 32);
            this.Reset.TabIndex = 8;
            this.Reset.Text = "Сбросить статистику";
            this.Reset.UseVisualStyleBackColor = false;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // PlayerName
            // 
            this.PlayerName.Location = new System.Drawing.Point(6, 19);
            this.PlayerName.Name = "PlayerName";
            this.PlayerName.Size = new System.Drawing.Size(210, 20);
            this.PlayerName.TabIndex = 9;
            // 
            // NamePl
            // 
            this.NamePl.Controls.Add(this.PlayerName);
            this.NamePl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NamePl.Location = new System.Drawing.Point(12, 191);
            this.NamePl.Name = "NamePl";
            this.NamePl.Size = new System.Drawing.Size(222, 56);
            this.NamePl.TabIndex = 6;
            this.NamePl.TabStop = false;
            this.NamePl.Text = "Имя";
            // 
            // langBox
            // 
            this.langBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.langBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.langBox.FormattingEnabled = true;
            this.langBox.Items.AddRange(new object[] {
            "Русский",
            "English"});
            this.langBox.Location = new System.Drawing.Point(6, 19);
            this.langBox.Name = "langBox";
            this.langBox.Size = new System.Drawing.Size(210, 21);
            this.langBox.TabIndex = 9;
            // 
            // Language
            // 
            this.Language.Controls.Add(this.langBox);
            this.Language.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Language.Location = new System.Drawing.Point(12, 254);
            this.Language.Name = "Language";
            this.Language.Size = new System.Drawing.Size(222, 56);
            this.Language.TabIndex = 10;
            this.Language.TabStop = false;
            this.Language.Text = "Язык";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 411);
            this.Controls.Add(this.Language);
            this.Controls.Add(this.NamePl);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.AIDifficult);
            this.Controls.Add(this.Theme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tic-Tac-Toe";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Settings_FormClosed);
            this.Theme.ResumeLayout(false);
            this.Theme.PerformLayout();
            this.AIDifficult.ResumeLayout(false);
            this.AIDifficult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Difficult)).EndInit();
            this.NamePl.ResumeLayout(false);
            this.NamePl.PerformLayout();
            this.Language.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Theme;
        private System.Windows.Forms.RadioButton Night;
        private System.Windows.Forms.RadioButton Day;
        private System.Windows.Forms.GroupBox AIDifficult;
        private System.Windows.Forms.TrackBar Difficult;
        private System.Windows.Forms.Label Hard;
        private System.Windows.Forms.Label Easy;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.TextBox PlayerName;
        private System.Windows.Forms.GroupBox NamePl;
        private System.Windows.Forms.ComboBox langBox;
        private System.Windows.Forms.GroupBox Language;
        private System.Windows.Forms.Label Middle;
    }
}